from core.models import Category
from rest_framework import serializers
from django.utils.text import slugify


class CategorySerializer(serializers.ModelSerializer):
    slug = serializers.ReadOnlyField()

    class Meta:
        model = Category
        fields = '__all__'

    def create(self, validated_data):
        if 'slug' not in validated_data:
            validated_data['slug'] = slugify(validated_data['category_name'])
        instance = super().create(validated_data)
        return instance
