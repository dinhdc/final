from api.serializers.categories import CategorySerializer
from api.serializers.products import ProductSerializer, ProductCUSerializer, VariantionWithProductSerializer, \
    VariantionWithoutProductSerializer
