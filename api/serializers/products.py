from core.models import Product, Variantion, ProductGallery, Category
from api.serializers.categories import CategorySerializer
from rest_framework import serializers
from django.utils.text import slugify


class VariantionWithoutProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variantion
        exclude = ('product',)


class VariantionWithProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variantion
        fields = '__all__'


class ProductBaseSerializer(serializers.ModelSerializer):
    slug = serializers.ReadOnlyField()

    class Meta:
        model = Product
        fields = '__all__'


class ProductGalleryImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductGallery
        fields = ('image',)


class ProductSerializer(ProductBaseSerializer):
    category = CategorySerializer()
    variantion_list = serializers.SerializerMethodField(method_name='get_variantion_list')
    galleries = serializers.SerializerMethodField()

    def get_variantion_list(self, instance):
        variations = Variantion.objects.filter(product=instance)
        return VariantionWithoutProductSerializer(variations, many=True).data

    def get_galleries(self, product):
        gallery_list = ProductGallery.objects.filter(product=product)
        return ProductGalleryImageSerializer(gallery_list, many=True).data


class ImageSerializer(serializers.Serializer):
    image = serializers.ImageField(allow_empty_file=False)


class ProductCUSerializer(serializers.ModelSerializer):
    slug = serializers.ReadOnlyField()

    class Meta:
        model = Product
        fields = ('product_name', 'description', 'image', 'price', 'stock',
                  'category', 'slug',)

    def create(self, validated_data):
        if 'slug' not in validated_data:
            validated_data['slug'] = slugify(validated_data['product_name'])
        product = Product.objects.create(**validated_data)
        return product
