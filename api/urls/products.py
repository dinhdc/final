from rest_framework.routers import DefaultRouter
from api.views.products import *

router = DefaultRouter()
router.register(r'products', ProductViewSet, basename='Product CRUD')
router.register(r'variations', VariationViewSet, basename='Variations CRUD')

urlpatterns = router.urls
