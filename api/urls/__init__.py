from django.urls import include, path

urlpatterns = [
    path('auth/', include('api.urls.auth')),
    path('', include('api.urls.categories')),
    path('', include('api.urls.products')),
]