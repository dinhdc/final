from rest_framework.routers import DefaultRouter
from api.views.categories import *

router = DefaultRouter()
router.register(r'categories', CategoryViewSet, basename='Category CRUD')

urlpatterns = router.urls
