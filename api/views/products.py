import json
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from api.serializers import *
from rest_framework.permissions import IsAdminUser, SAFE_METHODS
from core.models import Product, Variantion, ProductGallery


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [IsAdminUser]

    def create(self, request, *args, **kwargs):
        product_data = request.data.copy()
        gallery_list = product_data.pop('galleries', [])
        variation_list = product_data.pop('variations', [])
        serializer = ProductCUSerializer(data=product_data)
        if serializer.is_valid():
            product = serializer.save()
            for gallery in gallery_list:
                ProductGallery.objects.create(product=product, image=gallery)
            for variation in variation_list:
                variation_object = json.loads(variation)
                Variantion.objects.create(product=product, **variation_object)
            return Response(ProductSerializer(product, context={"request": request}).data,
                            status=status.HTTP_201_CREATED)
        else:
            return Response({"error": True, "message": serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return ProductSerializer
        else:
            return ProductCUSerializer


class VariationViewSet(ModelViewSet):
    queryset = Variantion.objects.all()
    serializer_class = VariantionWithProductSerializer
    permission_classes = [IsAdminUser]
