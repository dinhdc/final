from rest_framework.viewsets import ModelViewSet
from api.serializers import CategorySerializer
from rest_framework.permissions import IsAdminUser
from core.models import Category


class CategoryViewSet(ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = [IsAdminUser]
