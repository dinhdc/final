from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from django.views.static import serve

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('mvc.urls')),
    path('api/', include('api.urls')),
]

# Serve static files in development
if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Serve media files in production
if not settings.DEBUG:
    urlpatterns += [
        path('media/<path>', serve, {'document_root': settings.MEDIA_ROOT}),
        path('static/<path>', serve, {'document_root': settings.STATIC_ROOT}),
    ]
