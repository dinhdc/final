from .register import RegistrationForm, UserForm, UserProfileForm
from .order import OrderForm
from .rating import ReviewForm
