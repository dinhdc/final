from django import forms
from core.models import ReviewRating


class ReviewForm(forms.ModelForm):

    class Meta:
        model = ReviewRating
        fields = ['subject', 'review', 'rating']
