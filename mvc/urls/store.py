from django.urls import path
from mvc.views import store as views

urlpatterns = [
    path('store', views.store, name='store'),
    path('search', views.search, name='search'),
    path('store/<slug:category_slug>/', views.store, name='products_by_category'),
    path('store/<slug:category_slug>/<slug:product_slug>/',
         views.product_detail, name='product_detail'),
    path('submit-review/<int:product_id>/', views.submit_review, name='submit_review')
]
