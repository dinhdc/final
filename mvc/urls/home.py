from django.urls import path
from mvc.views import home as views

urlpatterns = [
    path('', views.home, name='home'),
]
