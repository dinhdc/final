from django.urls import include, path

urlpatterns = [
  path('', include('mvc.urls.home')),
  path('cart/', include('mvc.urls.cart')),
  path('', include('mvc.urls.store')),
  path('accounts/', include('mvc.urls.accounts')),
  path('orders/', include('mvc.urls.orders')),
]