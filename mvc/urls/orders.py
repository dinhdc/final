from django.urls import path
from mvc.views import orders as views


urlpatterns = [
  path('place-order/', views.place_order, name='place_order'),
  path('payment/', views.payment, name='payment'),
  path('order-completed/', views.order_completed, name='order_completed'),
]