from django.shortcuts import render
from core.models import *


def home(request):
    products = Product.objects.filter(is_available=True).order_by('-created_date')
    # for product in products:
    #     reviews = ReviewRating.objects.filter(product=product, status=True)
    context = {'products': products}
    return render(request, 'home.html', context)
