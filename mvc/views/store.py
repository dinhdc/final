from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect
from core.models import *
from core.utils import get_cart_id
from django.core.paginator import Paginator
from django.db.models import Q
from django.contrib import messages
from mvc.form import ReviewForm


def store(request, category_slug=None):
    if category_slug:
        categories = get_object_or_404(Category, slug=category_slug)
        products = Product.objects.filter(
            category=categories, is_available=True)
        paginator = Paginator(products, 1)
        page = request.GET.get('page', 1)
        paged_products = paginator.get_page(page)
    else:
        products = Product.objects.filter(is_available=True)
        paginator = Paginator(products, 3)
        page = request.GET.get('page', 1)
        paged_products = paginator.get_page(page)
    context = {'products': paged_products, 'product_count': products.count()}
    return render(request, 'store/store.html', context)


def product_detail(request, category_slug, product_slug):
    try:
        single_product = Product.objects.get(
            slug=product_slug, category__slug=category_slug)
        in_cart = CartItem.objects.filter(cart__cart_id=get_cart_id(
            request), product=single_product).exists()
    except Exception as e:
        raise e
    if request.user.is_authenticated:
        try:
            order_product = OrderProduct.objects.filter(user=request.user, product=single_product).exists()
        except Exception:
            order_product = None
    else:
        order_product = None

    product_gallery = ProductGallery.objects.filter(product=single_product)
    reviews = ReviewRating.objects.filter(product=single_product, status=True)
    context = {
        'product': single_product,
        'in_cart': in_cart,
        'ordered': order_product,
        'reviews': reviews,
        'product_gallery': product_gallery
    }
    return render(request, 'store/product_detail.html', context)


def search(request):
    products = Product.objects.all()
    if 'keyword' in request.GET:
        keyword = request.GET.get('keyword')
        if keyword:
            products = products.filter(
                Q(description__icontains=keyword) | Q(product_name__icontains=keyword)).order_by('-created_date')
    context = {'products': products, 'product_count': products.count()}
    return render(request, 'store/store.html', context)


def submit_review(request, product_id):
    url = request.META.get('HTTP_REFERER')
    if request.method == "POST":
        try:
            review = ReviewRating.objects.get(user=request.user, product__id=product_id)
            form = ReviewForm(request.POST)
            review.subject = form.cleaned_data['subject']
            review.rating = form.cleaned_data['rating']
            review.review = form.cleaned_data['review']
            review.save()
            messages.success(request, 'Cảm ơn! Đánh giá của bạn đã được ghi nhận.')
            return redirect(url)
        except ReviewRating.DoesNotExist:
            form = ReviewForm(request.POST)
            if form.is_valid():
                print("valid")
                data = ReviewRating()
                data.subject = form.cleaned_data['subject']
                data.rating = form.cleaned_data['rating']
                data.review = form.cleaned_data['review']
                data.ip = request.META.get('REMOTE_ADDR')
                data.product_id = product_id
                data.status = True
                data.user_id = request.user.id
                data.save()
                messages.success(request, 'Cảm ơn! Đánh giá của bạn đã được gửi.')
                return redirect(url)
