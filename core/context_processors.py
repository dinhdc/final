from core.models import *
from core.utils import get_cart_id


def menu_links(request):
    links = Category.objects.all()
    return dict(links=links)


def counter(request):
    if 'admin' in request.path:
        return {}
    else:
        try:
            cart_count = 0
            if request.user.is_authenticated:
                cart_items = CartItem.objects.filter(user=request.user)
            else:
                cart = Cart.objects.get(cart_id=get_cart_id(request))
                cart_items = CartItem.objects.filter(cart=cart)
            for cart_item in cart_items:
                cart_count += cart_item.quantity
        except Cart.DoesNotExist:
            cart_count = 0
    return dict(cart_count=cart_count)


def address(request):
    locations = LocalBranch.objects.all()
    return dict(locations=locations)
