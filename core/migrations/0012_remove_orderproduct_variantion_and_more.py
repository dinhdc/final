# Generated by Django 4.2 on 2023-04-26 16:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_remove_orderproduct_color_remove_orderproduct_size_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='orderproduct',
            name='variantion',
        ),
        migrations.AddField(
            model_name='orderproduct',
            name='variantion',
            field=models.ManyToManyField(to='core.variantion'),
        ),
    ]
