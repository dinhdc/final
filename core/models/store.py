from django.db import models


class LocalBranch(models.Model):
    company_name = models.CharField(max_length=255)
    email = models.EmailField(max_length=255)
    phone = models.CharField(max_length=255)
    address = models.CharField(max_length=255)
    activate = models.BooleanField(default=False)

    def __str__(self):
        return self.company_name
